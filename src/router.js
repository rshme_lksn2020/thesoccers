import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
    {
         path: '/',
        redirect: '/leagues'
    },

    {
        name: 'leagues',
        path: '/leagues',
        component: require('./views/Leagues').default
    },

    {
        name: 'detail_league',
        path: '/leagues/:idLeague',
        component: require('./components/Leagues/Detail').default,
        props:true
    },

    {
      name: 'detail_club',
      path: '/leagues/club/:idTeam',
      component: require('./components/Leagues/Club').default,
        props:true
    },

    {
        name: 'matchs',
        path: '/matchs',
        component: require('./views/Matchs').default
    }
]

export default new Router({
    routes
})