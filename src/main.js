import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)

Vue.filter('dateParser', function(date){
  return new Date(date).toLocaleDateString('en-us', {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  })
})

Vue.config.productionTip = false

window.axios = Axios
window.baseURL = 'https://www.thesportsdb.com/api/v1/json'
window.apiKey = '1'

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
